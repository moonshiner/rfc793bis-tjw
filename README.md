This repository is for working on an update to the TCP specification
(RFC 793).  It holds XML source, and other files that will be used to
produce an updated RFC.

Please use the IETF TCPM mailing list for discussion of this document.
https://www.ietf.org/mailman/listinfo/tcpm


